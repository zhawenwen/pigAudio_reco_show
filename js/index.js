/******
 * @ Author: qiaoyu
 * @ Date: 2022-08-19 13:03:05
 * @ LastEditTime: 2022-08-19 13:14:26
 * @ LastEditors: qiaoyu
 * @ Description:
 * @ FilePath: \pigAudio\js\index.js
 * @ Made by qiaoyukeji@gmail.com
 */
var fileInput = document.querySelector("#fileInput");
var myButton = document.getElementById("myButton");
var pig_fap = document.getElementById("pig_fap");
var pig_eat = document.getElementById("myButton");
var pig_estrous = document.getElementById("pig_estrous");
var pig_howl = document.getElementById("pig_howl");
var pig_oink = document.getElementById("pig_oink");
myBase64 = "0";
//   wav 生成base64
fileInput.onchange = function () {
  var file = this.files[0];
  var reader = new FileReader();
  reader.readAsDataURL(file);
  reader.onload = function () {
    myBase64 = reader.result.split(",")[1];
  };
};
myButton.onclick = function () {
  if (myBase64.length > 5) {
    console.log(myBase64);
    axios
      .post(
        "https://aip.baidubce.com/rpc/2.0/ai_custom/v1/sound_cls/pigAudio_001?access_token=24.95cc0e34797200a4a57e39b5f05cd44e.2592000.1663378318.282335-27063048",
        { sound: myBase64, top_num: 5 }
      )
      .then(
        function (response) {
          console.log(response);
          console.log(response.data.results);
          var results = response.data.results;
          for (let index = 0; index < results.length; index++) {
            // toFixed(6) 保留 6 位小数
            if (results[index].name == "pig_fap") {
              pig_fap.innerText = results[index].score.toFixed(8);
            } else if (results[index].name == "pig_oink") {
              pig_oink.innerText = results[index].score.toFixed(8);
            } else if (results[index].name == "pig_eat") {
              pig_eat.innerText = results[index].score.toFixed(8);
            } else if (results[index].name == "pig_howl") {
              pig_howl.innerText = results[index].score.toFixed(8);
            } else if (results[index].name == "pig_estrous") {
              pig_estrous.innerText = results[index].score.toFixed(8);
            }
          }
        },
        function (err) {
          console.log(err);
        }
      );

    //   var results = [
    //     {
    //       name: "pig_fap",
    //       score: 0.9999986886978149,
    //     },
    //     {
    //       name: "pig_eat",
    //       score: 0.0000013018020581512246,
    //     },
    //     {
    //       name: "pig_estrous",
    //       score: 4.274086151667689e-8,
    //     },
    //     {
    //       name: "pig_howl",
    //       score: 4.753773819032858e-9,
    //     },
    //     {
    //       name: "pig_oink",
    //       score: 7.098296950225347e-11,
    //     },
    //   ];
    //   for (let index = 0; index < results.length; index++) {
    //     // toFixed(6) 保留 6 位小数
    //     if (results[index].name == "pig_fap") {
    //       pig_fap.innerText = results[index].score;
    //     } else if (results[index].name == "pig_oink") {
    //       pig_oink.innerText = results[index].score;
    //     } else if (results[index].name == "pig_eat") {
    //       pig_eat.innerText = results[index].score;
    //     } else if (results[index].name == "pig_howl") {
    //       pig_howl.innerText = results[index].score;
    //     } else if (results[index].name == "pig_estrous") {
    //       pig_estrous.innerText = results[index].score;
    //     }
    //   }
  } else {
    alert("请先选择音频文件！！");
  }
};
// 时间计数
var t = null;
t = setTimeout(time, 1000); //開始运行
function time() {
  clearTimeout(t); //清除定时器
  dt = new Date();
  // console.log(dt);
  var y = dt.getFullYear();
  var mt = dt.getMonth() + 1;
  var day = dt.getDate();
  var h = dt.getHours(); //获取时
  var m = dt.getMinutes(); //获取分
  var s = dt.getSeconds(); //获取秒
  document.querySelector(".showTime").innerHTML =
    "当前时间：" +
    y +
    "年" +
    mt +
    "月" +
    day +
    "-" +
    h +
    "时" +
    m +
    "分" +
    s +
    "秒";
  t = setTimeout(time, 1000); //设定定时器，循环运行
}
